<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:50
 */

namespace App\Controller;


use App\Helper\Logger;
use App\Model\Storable;
use App\Model\Warehouse;
use App\Repository\Repository;

/**
 * Class SandboxFactory
 *
 * A demo class - test controller
 *
 * @package App\Controller
 */
class SandboxFactory
{
    use Logger;

    /**
     * Array of brand objects
     * @var array
     */
    public $brands      = [];
    /**
     * Array of warehouses
     * @var array
     */
    public $warehouses  = [];
    /**
     * array of ebook items
     * @var array
     */
    public $ebooks      = [];
    /**
     * array of paper book items
     * @var array
     */
    public $paperbooks  = [];
    /**
     * array of un-stored items
     * @var array
     */
    public $unstored    = [];


    /**
     * A Factory method to generate Models from the Config files
     *
     * @param $model
     */
    public function generateModels(string $model): void
    {
        // brandProperties is already checked, ready to use
        $repository = new Repository($model);
        $fields     = $repository->getRepositoryArray('fields');
        $values     = $repository->getRepositoryArray('values');

        list($fields, $values, $validity) = $this->checkValidity($fields, $values);
        if($validity) {
            foreach ($values as $classVariables) {
                $modelVariables = [];
                foreach ($fields as $classVariableName=>$types) {
                    $modelVariables[] = $classVariables[$classVariableName];
                }
                $modelName      = '\App\Model\\'.ucfirst($model);
                $classVariable  = $model.'s';
                $this->$classVariable[] = new $modelName(...$modelVariables);
            }
        } else {
            $this->log('Model('.ucfirst($model).') generation failed.', 'error');
        }
    }


    /**
     * Checks the validity of received data by its type
     *
     * @param $types
     * @param $values
     * @return array|bool
     */
    private function checkValidity(array $types,array $values): array
    {
        try {
            if(!is_array($types) || !is_array($values) || empty($types) || empty($values)) {
                throw new \UnexpectedValueException('Types and values must be a non-empty array');
            } else {
                foreach ($values as $row=>$inputValues) {
                    if(!is_array($inputValues)) {
                        throw new \UnexpectedValueException('InputValues must contain valid key-value pairs');
                    } else {
                        foreach ($inputValues as $key=>$value) {
                            if (!isset($types[$key])) {
                                throw new \UnexpectedValueException($key . ' is not exists in fields array.');
                            } else {
                                if ($types[$key] == 'string') {
                                    if (!is_string($value)) {
                                        throw new \InvalidArgumentException('Argument ' . $key . ' must be a string');
                                    }
                                } elseif ($types[$key] == 'int') {
                                    if (!is_int($value)) {
                                        throw new \InvalidArgumentException('Argument ' . $key . ' must be an integer');
                                    }
                                } elseif (class_exists($types[$key])) {
                                    $values[$row][$key] = $this->findObject($types[$key], $value, $key.'s');
                                }
                            }
                        }
                    }
                }
            }
        } catch (\UnexpectedValueException | \InvalidArgumentException $e) {
            $this->log($e->getMessage(), 'error');
            return false;
        }
        return [$types, $values, true];

    }

    /**
     * Returns an object from a class variable array by its name
     *
     * @param $class
     * @param $className
     * @param $variable
     * @return mixed
     */
    public function findObject($class, $className, $variable)
    {
        try {
            if (!is_array($this->$variable)) {
                throw new \UnexpectedValueException($variable.' array not exists. Generation stopped.');
            } else {
                foreach ($this->$variable as $object) {
                    if($object instanceof $class) {
                        if($object->getName() == $className) {
                            return $object;
                        }
                    }
                }
                throw new \InvalidArgumentException($className.' Does not exists. Generation stopped.');
            }
        } catch (\UnexpectedValueException | \InvalidArgumentException $e) {
            $this->log($e->getMessage(), 'error');
            die;
        }
    }

    /**
     * Stores the items in the warehouses
     *
     * @param Storable $item
     * @return bool
     */
    public function storeItem(Storable $item): bool
    {
        try {
            if (!$item instanceof Storable) {
                throw new \UnexpectedValueException('Item must be storable to store');
            } else {
                if(!is_array($this->warehouses)) {
                    throw new \UnexpectedValueException('There is no warehouse.');
                } else {
                    foreach ($this->warehouses as $key=>$warehouse) {
                        if(!$warehouse instanceof Warehouse) {
                            throw new \InvalidArgumentException('Warehouse object must be an instance of Warehouse');
                        } else {
                            $freeCapacity   = $warehouse->getCapacity() - $warehouse->getQuantitiesInStock();
                            $requiredQty    = $requiredQty ?? $item->getQuantity();
                            //if there is enough free capacity to store the item
                            if($requiredQty > 0) {
                                if ($freeCapacity >= $item->getQuantity()) {
                                    $this->warehouses[$key]->addItemToStock($item);
                                    $requiredQty = 0;
                                } else {
                                    if ($freeCapacity > 0) {
                                        $newItem = clone $item;
                                        $newItem->setQuantity($freeCapacity);
                                        $item->reduceQuantity($freeCapacity);
                                        $this->warehouses[$key]->addItemToStock($newItem);
                                        $requiredQty -= $freeCapacity;
                                    }
                                }
                            } else {
                                return true;
                            }
                        }
                    }
                    if(isset($requiredQty) && $requiredQty > 0) {
                        $this->unstored[] = $item;
                        return false;
                    }
                }
            }
        } catch (\UnexpectedValueException | \InvalidArgumentException $e) {
             $this->log($e->getMessage(), 'error');
        }
    }

    /**
     *  Generates the list of the content of the warehouses
     */
    public function listWarehouses(): void
    {
        try {
            if(!is_array($this->warehouses) || empty($this->warehouses)) {
                throw new \InvalidArgumentException('There is no warehouses initialized');
            } else {
                foreach ($this->warehouses as $warehouse) {
                    if(!$warehouse instanceof Warehouse) {
                        throw new \UnexpectedValueException('Warehouse must be an instance of Warehouse');
                    } else {
                        /** @var $warehouse Warehouse */
                        $this->log("\n".$warehouse->getName().' (capacity:'.$warehouse->getCapacity().')', 'success');
                        $this->log("\tUsed:".$warehouse->getCapacity().'/'.$warehouse->getQuantitiesInStock());
                        $this->log("\tContains the following Items:");
                        if(is_array($warehouse->getInStock()) && !empty($warehouse->getInStock())) {
                            foreach ($warehouse->getInStock() as $item) {
                                $this->log("\t".$item->getName().' ('.$item->getQuantity().' qty)');
                            }
                        }
                    }
                }
            }
        } catch (\InvalidArgumentException | \UnexpectedValueException $e) {
            $this->log($e, 'error');
        }
    }


    /**
     *  Lists the un-stored items
     */
    public function listUnStoredItems(): void
    {
        if(is_array($this->unstored) && !empty($this->unstored)) {
            $this->log("\n\nUnable to store the following items:", 'warning');
            foreach ($this->unstored as $item) {
                $this->log("\t".$item->getName().' ('.$item->getQuantity().' qty)');
            }
        }
    }


}