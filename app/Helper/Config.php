<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:50
 */

namespace App\Helper;

/**
 * Class Config
 * A very simple config class
 *
 * @package App\Helper
 */
class Config {

    /**
     * Array of config items of the used config file
     * @var array
     */
    public static $items = array();
    /**
     * Config path
     * @var string
     */
    protected static $configPath    = 'config/';
    /**
     * Config file extension
     * @var string
     */
    protected static $extension     = '.php';

    /**
     * Loads the required config file, and return the array of config items
     * @param $fileName
     */
    public static function load($fileName): void
    {
        try {
            if(!is_file(static::$configPath.$fileName.static::$extension)) {
                throw new \Exception('Config file ['.static::$configPath.$fileName.static::$extension.'] not exists. Program terminated.');
            }
            static::$items = include(static::$configPath.$fileName.static::$extension);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        }
    }

    /**
     * returns the required config parameters
     *
     * @param null $key
     * @return array|mixed
     */
    public static function get($key = null): array
    {
        $input = explode('.', $key);
        $fileName = explode('.', $key)[0];
        unset($input[0]);
        $key = implode('.', $input);

        static::load($fileName);

        if (!empty($key))
        {
            return static::$items[$key] ?? [];
        }

        return static::$items;
    }

}