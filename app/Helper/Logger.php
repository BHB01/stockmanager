<?php
namespace App\Helper;


/**
 * Trait Logger
 * Generates the CLI messages and detailed.log
 *
 * @package Language\Traits
 */
trait Logger
{

    /**
     * Generates inline CLI messages
     *
     * @param $message
     * @param string $type
     */
    public function log($message, $type=''): void
    {
        switch ($type) {
            case 'error':
                echo "\e[31m\nERROR \n\e[0m"."\e[1;33m$message\e[0m\n";
                break;
            case 'warning':
                echo "\e[1;33m$message\e[0m\n";
                break;
            case 'success':
                echo "\e[32m$message\e[0m\n";
                break;
            default:
                echo $message."\n";
                break;

        }
    }

    /**
     * Generates a log file (./detailed.log) that contains all the Stack traces of the errors from exceptions
     * @param $message
     * @param bool $isException
     */
    public function logFile($message, $isException=false):void 
    {
        $content = '';
        $destination = '/detailed.log';
        if($isException===true) {
            $content .= $message->getMessage()."\n".$message->getTraceAsString()."\n";
        } else {
            $content .= $message."\n";
        }
        try {
            if (!file_put_contents($destination, $content, FILE_APPEND)) {
                throw new \UnexpectedValueException('No permission to write a log file.');
            }
        } catch(\Exception $e) {
            $this->log($e->getMessage());
        }
    }
}
