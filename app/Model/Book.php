<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:55
 */

namespace App\Model;


/**
 * Class Book
 * contains some not item relevant variables
 * it could be an interface in this example, but in real life it must have abstract functions for the children
 * @package App\Model
 */
abstract class Book extends Item
{
    /**
     * @var
     */
    protected $author;
    /**
     * @var
     */
    protected $yearOfPublication;

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getYearOfPublication()
    {
        return $this->yearOfPublication;
    }

    /**
     * @param mixed $yearOfPublication
     */
    public function setYearOfPublication($yearOfPublication)
    {
        $this->yearOfPublication = $yearOfPublication;
    }


}