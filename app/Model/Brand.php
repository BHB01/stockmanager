<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 18:04
 */

namespace App\Model;


/**
 * Class Brand
 *
 * @package App\Model
 */
class Brand
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    public $quality;


    /**
     * Brand constructor.
     * @param string $name
     * @param int $quality
     */
    public function __construct(string $name, int $quality)
    {
        $this->name     = $name;
        $this->quality  = $quality;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }



}