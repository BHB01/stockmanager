<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:54
 */

namespace App\Model;


/**
 * Interface Downloadable
 * It is not used in this example, but its use in a real application could be relevant
 *
 * @package App\Model
 */
interface Downloadable
{
    /**
     * @return mixed
     */
    public function download();
}