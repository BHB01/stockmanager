<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:55
 */

namespace App\Model;


/**
 * Class EBook
 * @package App\Model
 */
class EBook extends Book implements Sellable, Downloadable
{

    /**
     * @var
     */
    protected $size;
    /**
     * @var
     */
    protected $type;


    /**
     * EBook constructor.
     * Creates an e-book item
     *
     * @param string $title
     * @param string $ean
     * @param float $price
     * @param Brand $brand
     * @param string $author
     * @param int $yearOfPublication
     * @param int $quantity
     */
    public function __construct(string $title, string $ean, float $price, Brand $brand, string $author, int $yearOfPublication, int $quantity)
    {
        $this->title    = $title;
        $this->ean      = $ean;
        $this->price    = $price;
        $this->brand    = $brand;
        $this->author   = $author;
        $this->yearOfPublication = $yearOfPublication;
        $this->quantity = $quantity;
    }

    /**
     * @return mixed|void
     */
    public function download()
    {
        // TODO: Implement download() method.
    }

    /**
     * @return mixed|void
     */
    public function sell()
    {
        // TODO: Implement sell() method.
    }
}