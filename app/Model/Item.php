<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:55
 */

namespace App\Model;


abstract class Item
{
    protected $title;
    protected $ean;
    protected $price;
    protected $brand;
    protected $quantity;


    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * @param mixed $ean
     */
    public function setEan($ean)
    {
        $this->ean = $ean;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    public function getName() {
        return $this->getTitle();
    }

    public function reduceQuantity($number)
    {
        $newQuantity = $this->getQuantity()-$number;
        if($newQuantity < 0 ) {
            $newQuantity = 0;
        }

        $this->setQuantity($newQuantity);
    }


}