<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:56
 */

namespace App\Model;


final class PaperBook extends Book implements Sellable, Storable
{
    private $condition;
    private $weight;


    public function __construct(string $title, string $ean,float $price, Brand $brand, string $author, int $yearOfPublication,int $quantity)
    {
        $this->title    = $title;
        $this->ean      = $ean;
        $this->price    = $price;
        $this->brand    = $brand;
        $this->author   = $author;
        $this->yearOfPublication = $yearOfPublication;
        $this->quantity = $quantity;
    }

    public function sell()
    {
        // TODO: Implement sell() method.
    }


    /**
     * @return mixed
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param mixed $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }



}