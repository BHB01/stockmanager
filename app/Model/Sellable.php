<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:55
 */

namespace App\Model;


/**
 * Interface Sellable
 * It is not used in this example, but its use in a real application could be relevant
 *
 * @package App\Model
 */
interface Sellable
{
    /**
     * @return mixed
     */
    public function sell();
}