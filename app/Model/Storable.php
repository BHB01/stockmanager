<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:53
 */

namespace App\Model;


/**
 * Interface Storable
 *
 * It the Item could be stored it must implement this interface
 * It could contain storage-relevant functions
 *
 * @package App\Model
 */
interface Storable
{

}