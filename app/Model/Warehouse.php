<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:56
 */

namespace App\Model;


use App\Helper\Logger;

class Warehouse
{
    use Logger;
    private $name;
    private $address;
    private $capacity;

    private $inStock;

    /**
     * Warehouse constructor.
     * @param $name
     * @param $address
     * @param $capacity
     */
    public function __construct($name, $address, $capacity)
    {
        $this->name = $name;
        $this->address = $address;
        $this->capacity = $capacity;
    }

    /**
     * @return mixed
     */
    public function getInStock()
    {
        return $this->inStock;
    }

    /**
     * @param mixed $inStock
     */
    public function setInStock($inStock): void
    {
        $this->inStock = $inStock;
    }


    /**
     * @return mixed
     */
    public function getCapacity()
    {
        return $this->capacity;
    }



    public function addItemToStock(Item $item): void {
        try {
            if(!$item  instanceof Item) {
                throw new \InvalidArgumentException('$item must be an instance of Item');
            } else {
                $this->inStock[] = $item;
            }

        } catch (\InvalidArgumentException $e) {
            $this->log($e->getMessage(), 'error');
        }

    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    public function getQuantitiesInStock(): int
    {
        $sumQty = 0;
        if(!empty($this->inStock)) {
            foreach ($this->inStock as $item) {
                $sumQty += $item->getQuantity();
            }
        }
        return $sumQty;
    }
}