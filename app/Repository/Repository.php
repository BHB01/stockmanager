<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:56
 */

namespace App\Repository;


use App\Helper\Config;
use App\Helper\Logger;

class Repository
{
    use Logger;
    protected $repository;

    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    public function getRepository() {
        return $this->repository;
    }

    public function getRepositoryArray($key) {
        $values = [];
        try {
            $values = Config::get($this->repository.'.'.$key);
            if(!is_array($values)) {
                throw new \TypeError(ucfirst($this->repository).' properties is not an array');
            }
            if(empty($values)) {
                throw new UnexpectedValueException(ucfirst($this->repository).' properties array is empty');
            }
        } catch(\TypeError $e) {
            $this->log($e->getMessage(), 'ERROR');
        } catch (\UnexpectedValueException $e) {
            $this->log($e->getMessage(), 'ERROR');
        }
        return $values;
    }

    protected function add() {}
    protected function delete() {}
    protected function update() {}

}