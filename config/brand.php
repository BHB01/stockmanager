<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 20:02
 */

return [
    'values' =>
    [
        ['name' => 'Brand A', 'quality' => 1],
        ['name' => 'Brand B', 'quality' => 2],
        ['name' => 'Brand C', 'quality' => 3],
        ['name' => 'Brand D', 'quality' => 4],
        ['name' => 'Brand E', 'quality' => 5],
    ],
    'fields' =>
    [
        'name'=>'string',
        'quality'=>'int',
    ]

];