<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 20:02
 */


return [
    'values' =>
    [
        ['title' => 'Default e-Book 1', 'ean' => 'DEFEB1', 'price'=>100.10, 'brand' => 'Brand A', 'author' => 'Author Name', 'yearOfPublication' => 1990, 'quantity' => 5],
        ['title' => 'Default e-Book 2', 'ean' => 'DEFEB2', 'price'=>200.20, 'brand' => 'Brand B', 'author' => 'Author Name', 'yearOfPublication' => 1970, 'quantity' => 10],
        ['title' => 'Default e-Book 3', 'ean' => 'DEFEB3', 'price'=>300.30, 'brand' => 'Brand C', 'author' => 'Author Name', 'yearOfPublication' => 1960, 'quantity' => 20],
        ['title' => 'Default e-Book 4', 'ean' => 'DEFEB4', 'price'=>400.40, 'brand' => 'Brand A', 'author' => 'Author Name', 'yearOfPublication' => 1950, 'quantity' => 15],
        ['title' => 'Default e-Book 5', 'ean' => 'DEFEB5', 'price'=>500.50, 'brand' => 'Brand E', 'author' => 'Author Name', 'yearOfPublication' => 1996, 'quantity' => 30],
        ['title' => 'Default e-Book 6', 'ean' => 'DEFEB6', 'price'=>600.60, 'brand' => 'Brand D', 'author' => 'Author Name', 'yearOfPublication' => 1997, 'quantity' => 50],

    ],
    'fields' =>
    [
        'title'     =>'string',
        'ean'       =>'string',
        'price'     =>'float',
        'brand'     =>\App\Model\Brand::class,
        'author'    =>'string',
        'quantity'  =>'int',
        'yearOfPublication'=>'int'
    ]
];