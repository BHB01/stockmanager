<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 20:02
 */


return [
    'values' =>
        [
            ['title' => 'Default Paper book1', 'ean' => 'DEFPB1', 'price'=>100.10, 'brand' => 'Brand A', 'author' => 'Analog Writer', 'yearOfPublication' => 1991, 'quantity' => 25],
            ['title' => 'Default Paper book2', 'ean' => 'DEFPB2', 'price'=>200.20, 'brand' => 'Brand B', 'author' => 'Analog Writer', 'yearOfPublication' => 1972, 'quantity' => 15],
            ['title' => 'Default Paper book3', 'ean' => 'DEFPB3', 'price'=>300.30, 'brand' => 'Brand C', 'author' => 'Analog Writer', 'yearOfPublication' => 1984, 'quantity' => 40],
            ['title' => 'Default Paper book4', 'ean' => 'DEFPB4', 'price'=>400.40, 'brand' => 'Brand A', 'author' => 'Analog Writer', 'yearOfPublication' => 1994, 'quantity' => 35],
            ['title' => 'Default Paper book5', 'ean' => 'DEFPB5', 'price'=>500.50, 'brand' => 'Brand E', 'author' => 'Analog Writer', 'yearOfPublication' => 2010, 'quantity' => 20],
            ['title' => 'Default Paper book6', 'ean' => 'DEFPB6', 'price'=>600.60, 'brand' => 'Brand D', 'author' => 'Analog Writer', 'yearOfPublication' => 2020, 'quantity' => 150],

        ],
    'fields' =>
        [
            'title'     =>'string',
            'ean'       =>'string',
            'price'     =>'float',
            'brand'     =>\App\Model\Brand::class,
            'author'    =>'string',
            'yearOfPublication'=>'int',
            'quantity'  =>'int',
        ]
];