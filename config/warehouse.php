<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 20:02
 */

return [
    'values' =>
        [
            ['name' => 'Warehouse A', 'address' => 'City A, Street A', 'capacity' => 20],
            ['name' => 'Warehouse B', 'address' => 'City B, Street B', 'capacity' => 40],
            ['name' => 'Warehouse C', 'address' => 'City C, Street C', 'capacity' => 80],
        ],
    'fields' =>
        [
            'name'=>'string',
            'address'=>'string',
            'capacity'=>'int'
        ]
];