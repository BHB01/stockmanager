<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 15.
 * Time: 17:58
 */

include "vendor/autoload.php";

$sandBox     = new \App\Controller\SandboxFactory();
// generate predefined and required models
$sandBox->generateModels('brand');
$sandBox->generateModels('warehouse');
$sandBox->generateModels('ebook');
$sandBox->generateModels('paperbook');

$paperBookToStore = $sandBox->findObject(\App\Model\PaperBook::class, 'Default Paper book1', 'paperbooks');
$sandBox->storeItem($paperBookToStore);
$paperBook2ToStore = $sandBox->findObject(\App\Model\PaperBook::class, 'Default Paper book6', 'paperbooks');
$sandBox->storeItem($paperBook2ToStore);


$sandBox->listWarehouses();
$sandBox->listUnStoredItems();
