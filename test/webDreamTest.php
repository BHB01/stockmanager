<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 10. 16.
 * Time: 18:15
 */


use PHPUnit\Framework\TestCase;

final class webDreamTest  extends TestCase
{
    public function testWarehouseCreation()
    {
        $sandBox     = new \App\Controller\SandboxFactory();
        $sandBox->generateModels('warehouse');
        $this->assertContainsOnlyInstancesOf(\App\Model\Warehouse::class, $sandBox->warehouses);
    }

    public function testWriteWarehouseContent()
    {
        $sandBox     = new \App\Controller\SandboxFactory();
        $sandBox->generateModels('brand');
        $sandBox->generateModels('warehouse');
        $sandBox->generateModels('paperbook');
        $paperBookToStore = $sandBox->findObject(\App\Model\PaperBook::class, 'Default Paper book1', 'paperbooks');
        $sandBox->storeItem($paperBookToStore);
        ob_start();
        $sandBox->listWarehouses();
        $contentToCheck = ob_get_contents();
        ob_get_clean();
        $this->assertSame(294, strlen($contentToCheck));
    }


    public function testWarehouseContainsItem() {
        $sandBox     = new \App\Controller\SandboxFactory();
        $sandBox->generateModels('brand');
        $sandBox->generateModels('warehouse');
        $sandBox->generateModels('paperbook');
        $paperBookToStore = $sandBox->findObject(\App\Model\PaperBook::class, 'Default Paper book1', 'paperbooks');
        $sandBox->storeItem($paperBookToStore);
        $this->assertContainsOnlyInstancesOf(\App\Model\PaperBook::class, $sandBox->warehouses[0]->getInStock());
    }


    public function testUnableToStoreItem() {
        $sandBox     = new \App\Controller\SandboxFactory();
        $sandBox->generateModels('brand');
        $sandBox->generateModels('warehouse');
        $sandBox->generateModels('paperbook');
        $paperBookToStore = $sandBox->findObject(\App\Model\PaperBook::class, 'Default Paper book6', 'paperbooks');
        $sandBox->storeItem($paperBookToStore);
        $this->assertContainsOnlyInstancesOf(\App\Model\PaperBook::class, $sandBox->unstored);
    }
}